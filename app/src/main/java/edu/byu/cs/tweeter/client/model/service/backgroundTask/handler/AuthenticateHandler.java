package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.AuthenticateObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.AuthenticateTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.BackgroundTask;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class AuthenticateHandler extends BaseHandler<AuthenticateObserver>{
    String message;
    public AuthenticateHandler(AuthenticateObserver observer, String message) {
        super(observer);
        this.message = message;

    }
    @Override
    protected void runSuccessAction(Message msg) {
        User user = (User) msg.getData().getSerializable(AuthenticateTask.USER_KEY);
        AuthToken authToken = (AuthToken) msg.getData().getSerializable(AuthenticateTask.AUTH_TOKEN_KEY);
        getObserver().actionSucceeded(authToken, user);

    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to  " + this.message + ": " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to " + this.message + " because of exception: " + ex);
    }
}
