package edu.byu.cs.tweeter.client.model.service;

public interface SimpleNotificationObserver extends BaseObserver {
    void actionSucceeded();
}
