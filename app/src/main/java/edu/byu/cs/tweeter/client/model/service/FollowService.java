package edu.byu.cs.tweeter.client.model.service;

import edu.byu.cs.tweeter.client.model.service.backgroundTask.BackgroundTaskUtils;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.FollowTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFollowersCountTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFollowersTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFollowingCountTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFollowingTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.IsFollowerTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.UnfollowTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.BooleanHandler;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.GetCountHandler;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.IsFollowerHandler;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.PagedHandler;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class FollowService {

    public void loadMoreItemsFollowing(AuthToken currUserAuthToken, User user, int pageSize, User lastFollowee, PagedObserver observer) {
        GetFollowingTask getFollowingTask = new GetFollowingTask(currUserAuthToken,
                user, pageSize, lastFollowee, new PagedHandler<User>(observer, "following") {
        });
        BackgroundTaskUtils.runTask(getFollowingTask);
    }

    public void loadMoreItemsFollowers(AuthToken currUserAuthToken, User user, int pageSize, User lastFollower, PagedObserver observer) {
        GetFollowersTask getFollowersTask = new GetFollowersTask(currUserAuthToken,
                user, pageSize, lastFollower, new PagedHandler<User>(observer, "followers") {
        });
        BackgroundTaskUtils.runTask(getFollowersTask);
    }

    public void updateUserFollowers(AuthToken currUserAuthToken, User user, CountObserver observer) {
        // Get count of most recently selected user's followers.
        GetFollowersCountTask followersCountTask = new GetFollowersCountTask(currUserAuthToken,
                user, new GetCountHandler(observer, "get followers") {
        });
        BackgroundTaskUtils.runTask(followersCountTask);
    }

    public void updateUserFollowing(AuthToken currUserAuthToken, User user, CountObserver observer) {
        // Get count of most recently selected user's followees (who they are following)
        GetFollowingCountTask followingCountTask = new GetFollowingCountTask(currUserAuthToken,
                user, new GetCountHandler(observer, "get following") {
        });
        BackgroundTaskUtils.runTask(followingCountTask);
    }

    public void isFollower(AuthToken currUserAuthToken, User user, User selectedUser, IsFollowerObserver observer) {
        IsFollowerTask isFollowerTask = new IsFollowerTask(currUserAuthToken, user, selectedUser, new IsFollowerHandler(observer));
        BackgroundTaskUtils.runTask(isFollowerTask);
    }

    public void followUser(AuthToken currUserAuthToken, User selectedUser, BoolObserver observer ) {
        FollowTask followTask = new FollowTask(currUserAuthToken,
                selectedUser, new BooleanHandler(observer, "follow") {
        });
        BackgroundTaskUtils.runTask(followTask);
    }

    public void unFollowUser(AuthToken currUserAuthToken, User selectedUser, BoolObserver observer) {
        UnfollowTask unfollowTask = new UnfollowTask(currUserAuthToken,
                selectedUser, new BooleanHandler(observer, "unfollow") {
        });
        BackgroundTaskUtils.runTask(unfollowTask);
    }

}
