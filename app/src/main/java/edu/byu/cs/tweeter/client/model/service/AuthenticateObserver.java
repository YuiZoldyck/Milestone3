package edu.byu.cs.tweeter.client.model.service;

import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public interface AuthenticateObserver extends BaseObserver  {
    void actionSucceeded(AuthToken authToken, User user);
}
