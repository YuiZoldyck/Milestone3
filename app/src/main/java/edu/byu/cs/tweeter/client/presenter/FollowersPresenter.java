package edu.byu.cs.tweeter.client.presenter;

import java.util.List;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.GetUserObserver;
import edu.byu.cs.tweeter.client.model.service.PagedObserver;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class FollowersPresenter extends PagedPresenter<User>{
    public FollowersPresenter(PagedView view) {
        super(view);
    }

    @Override
    protected void loadMoreItems(AuthToken currUserAuthToken, User user, int pageSize, User lastItem, ServiceObserver serviceObserver) {
        followService.loadMoreItemsFollowers(Cache.getInstance().getCurrUserAuthToken(),
                user, getPageSize(), lastItem, new ServiceObserver());
    }

}
