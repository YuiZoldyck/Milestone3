package edu.byu.cs.tweeter.client.model.service;

import edu.byu.cs.tweeter.model.domain.User;

public interface GetUserObserver extends BaseObserver {
    void actionSucceeded(User user);
}
