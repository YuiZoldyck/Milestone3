package edu.byu.cs.tweeter.client.presenter;

import java.util.List;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.GetUserObserver;
import edu.byu.cs.tweeter.client.model.service.PagedObserver;
import edu.byu.cs.tweeter.client.model.service.StatusService;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;

public class StoryPresenter extends PagedPresenter<Status> {
    public StoryPresenter(PagedView view) {
        super(view);
    }

    @Override
    protected void loadMoreItems(AuthToken currUserAuthToken, User user, int pageSize, Status lastItem, ServiceObserver serviceObserver) {
        statusService.loadMoreStories(Cache.getInstance().getCurrUserAuthToken(), user,
                getPageSize(), lastItem, new ServiceObserver());
    }
}
