package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.IsFollowerObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.IsFollowerTask;

public class IsFollowerHandler extends BaseHandler<IsFollowerObserver> {

    public IsFollowerHandler(IsFollowerObserver observer) {
        super(observer);
    }

    @Override
    protected void runSuccessAction(Message msg) {
        boolean isFollower = msg.getData().getBoolean(IsFollowerTask.IS_FOLLOWER_KEY);

        getObserver().setButton(isFollower);
    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to determine following relationship: " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to determine following relationship because of exception: " + ex);
    }
}