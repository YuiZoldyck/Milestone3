package edu.byu.cs.tweeter.client.presenter;

import java.util.List;

import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;

public interface PagedView<T> extends BaseView {
    void addMoreItems(List<T> items);
    void setLoadingFooter(boolean value);
    void openMainView(User user);

}
