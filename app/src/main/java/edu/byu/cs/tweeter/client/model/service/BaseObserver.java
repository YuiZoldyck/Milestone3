package edu.byu.cs.tweeter.client.model.service;

public interface BaseObserver {
    void displayException(Exception ex);
    void displayError(String message);
}
