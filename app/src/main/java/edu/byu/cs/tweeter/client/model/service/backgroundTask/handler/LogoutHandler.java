package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.SimpleNotificationObserver;

public class LogoutHandler extends BaseHandler<SimpleNotificationObserver> {


    public LogoutHandler(SimpleNotificationObserver observer) {
        super(observer);
    }

    @Override
    protected void runSuccessAction(Message msg) {
        getObserver().actionSucceeded();
    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to logout: " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to logout because of exception: " + ex);
    }
}
