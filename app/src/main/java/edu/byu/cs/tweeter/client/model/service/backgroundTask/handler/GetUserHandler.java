package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.GetUserObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetUserTask;
import edu.byu.cs.tweeter.model.domain.User;

/**
 * Message handler (i.e., observer) for GetUserTask.
 */

public class GetUserHandler extends BaseHandler<GetUserObserver> {

    public GetUserHandler(GetUserObserver getUserObserver) {
        super(getUserObserver);
    }
    @Override
    protected void runSuccessAction(Message msg) {
        User user = (User) msg.getData().getSerializable(GetUserTask.USER_KEY);
        getObserver().actionSucceeded(user);
    }

    @Override
    protected void displayError(String message) {
        getObserver().displayError("Failed to get user's profile: " + message);
    }

    @Override
    protected void displayException(String ex) {
        getObserver().displayError("Failed to get user's profile because of exception: " + ex);
    }
}