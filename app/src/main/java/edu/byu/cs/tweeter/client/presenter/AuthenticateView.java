package edu.byu.cs.tweeter.client.presenter;

import edu.byu.cs.tweeter.model.domain.User;

public interface AuthenticateView extends ExtendedView{
    void openMainView(User user);
}
