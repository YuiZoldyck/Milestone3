package edu.byu.cs.tweeter.client.model.service;

import android.os.Message;

public interface IsFollowerObserver extends BaseObserver {
    void setButton(boolean value);
}
