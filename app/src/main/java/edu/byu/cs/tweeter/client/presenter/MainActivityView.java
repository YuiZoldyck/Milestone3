package edu.byu.cs.tweeter.client.presenter;

public interface MainActivityView extends BaseView {
    void openMainView();
    void setIsFollowingButton();
    void setIsNotFollowingButton();
    void setFollowingCount(int count);
    void setFollowersCount(int count);
    void showLogoutMessage(String message);
}
