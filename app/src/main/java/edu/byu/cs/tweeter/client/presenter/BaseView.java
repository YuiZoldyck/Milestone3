package edu.byu.cs.tweeter.client.presenter;

import edu.byu.cs.tweeter.model.domain.User;

public interface BaseView {
    void displayMessage(String message);
    void showErrorMessage(String error);
}
