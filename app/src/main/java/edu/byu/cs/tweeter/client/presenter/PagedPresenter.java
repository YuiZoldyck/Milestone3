package edu.byu.cs.tweeter.client.presenter;

import java.util.List;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.GetUserObserver;
import edu.byu.cs.tweeter.client.model.service.PagedObserver;
import edu.byu.cs.tweeter.client.model.service.StatusService;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public abstract class PagedPresenter<T> extends BasePresenter <PagedView> {
    public static int getPageSize() {
        return PAGE_SIZE;
    }

    private static final int PAGE_SIZE = 10;
    private T lastItem;
    private boolean hasMorePages;
    private boolean isLoading = false;

    public PagedPresenter(PagedView view) {
        super(view);
    }

    public boolean hasMorePages() {
        return hasMorePages;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public T getLastItem() {return lastItem;}

    public void loadMoreItems(User item) {
        if (!isLoading) {   // This guard is important for avoiding a race condition in the scrolling code.
            isLoading = true;
            view.setLoadingFooter(true);
            loadMoreItems(Cache.getInstance().getCurrUserAuthToken(), item,
                    PAGE_SIZE,  lastItem, new ServiceObserver());
        }
    }

    protected abstract void loadMoreItems(AuthToken currUserAuthToken, User user, int pageSize, T lastItem, ServiceObserver serviceObserver);

    public void getUsers(String alias) {
        userService.getUser(Cache.getInstance().getCurrUserAuthToken(), alias, new ServiceObserver());
    }

    public class ServiceObserver implements PagedObserver<T>, GetUserObserver {

        @Override
        public void displayException(Exception ex) {
            isLoading = false;
            view.setLoadingFooter(false);
            view.displayMessage(ex.getMessage());
        }

        @Override
        public void displayError(String message) {
            isLoading = false;
            view.setLoadingFooter(false);
            view.displayMessage(message);
        }

        @Override
        public void addMoreItems(List<T> items, boolean hasMorePages) {
            isLoading = false;
            view.setLoadingFooter(false);
            PagedPresenter.this.hasMorePages = hasMorePages;
            lastItem = (items.size() > 0) ? items.get(items.size() - 1) : null;
            view.addMoreItems(items);
        }

        @Override
        public void actionSucceeded(User user) {
            view.openMainView(user);
        }
    }
}
