package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import java.util.List;

import edu.byu.cs.tweeter.client.model.service.PagedObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFeedTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.PagedTask;
import edu.byu.cs.tweeter.model.domain.Status;

public abstract class PagedHandler<T> extends BaseHandler<PagedObserver> {
    String message;
    public PagedHandler(PagedObserver<T> observer, String message) {
        super(observer);
        this.message = message;
    }

    @Override
    protected void runSuccessAction(Message msg) {
        List<T> items = (List<T>) msg.getData().getSerializable(PagedTask.ITEMS_KEY);
        boolean hasMorePages = msg.getData().getBoolean(PagedTask.MORE_PAGES_KEY);
        getObserver().addMoreItems(items, hasMorePages);
    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to get " + this.message + ": " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to get " + this.message + ": " + ex);
    }
}
