package edu.byu.cs.tweeter.client.presenter;

import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.StatusService;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.User;

public abstract class BasePresenter<T extends BaseView> {

    protected final T view;
    protected final UserService userService;
    protected final FollowService followService;
    protected final StatusService statusService;
    protected BasePresenter(T view) {
        this.view = view;
        userService = getUserService();
        followService = getFollowService();
        statusService = getStatusService();
    }

    public FollowService getFollowService() {
        if (followService == null) {
            return new FollowService();
        }
        return followService;
    }

    public StatusService getStatusService() {
        if (statusService == null) {
            return new StatusService();
        }
        return statusService;
    }

    protected UserService getUserService() {
        if (userService == null) {
            return new UserService();
        }
        return userService;
    }
}
