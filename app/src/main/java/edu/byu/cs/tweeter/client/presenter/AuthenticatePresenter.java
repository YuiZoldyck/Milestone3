package edu.byu.cs.tweeter.client.presenter;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.AuthenticateObserver;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class AuthenticatePresenter extends BasePresenter<AuthenticateView> implements AuthenticateObserver {
    protected AuthenticatePresenter(AuthenticateView view) {
        super(view);
    }

    @Override
    public void actionSucceeded(AuthToken authToken, User user) {
        view.hideErrorMessage();
        Cache.getInstance().setCurrUser(user);
        Cache.getInstance().setCurrUserAuthToken(authToken);
        view.hideMessage();
        view.displayMessage("Hello, " + user.getName());
        view.openMainView(user);
    }

    @Override
    public void displayException(Exception ex) {
        view.showErrorMessage(ex.getMessage());
    }

    @Override
    public void displayError(String message) {
        view.showErrorMessage(message);
    }
}
