package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.CountObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetCountTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFollowingCountTask;

public abstract class GetCountHandler extends BaseHandler<CountObserver> {
    String message;
    public GetCountHandler(CountObserver observer, String message) {
        super(observer);
        this.message = message;
    }

    @Override
    protected void runSuccessAction(Message msg) {
        int count = msg.getData().getInt(GetCountTask.COUNT_KEY);
        if(message == "get following") {
            getObserver().setFollowingCount(count);
        } else {
            getObserver().setFollowersCount(count);
        }
    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to get " + this.message + " count: " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to get " + this.message + " count because of exception: " + ex);
    }
}
