package edu.byu.cs.tweeter.client.model.service;

public interface CountObserver extends BaseObserver {
    void setFollowersCount(int count);
    void setFollowingCount(int count);
}
