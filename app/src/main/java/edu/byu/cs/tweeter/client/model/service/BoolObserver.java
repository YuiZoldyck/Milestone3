package edu.byu.cs.tweeter.client.model.service;

public interface BoolObserver extends BaseObserver {
    void updateFollowButton(boolean b);
}
