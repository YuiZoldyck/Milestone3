package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.BoolObserver;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.AuthenticatedTask;

public abstract class BooleanHandler extends BaseHandler<BoolObserver> {
    private final String message;

    public BooleanHandler(BoolObserver observer, String message) {
        super(observer);
        this.message = message;
    }
    @Override
    protected void runSuccessAction(Message msg) {
//        getObserver().updateFollowButton(boolean b);
    }
    @Override
    protected void displayError(String message) {
        displayError("Failed to  " + this.message + ": " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to " + this.message + " because of exception: " + ex);
    }
}