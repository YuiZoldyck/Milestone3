package edu.byu.cs.tweeter.client.model.service;

import edu.byu.cs.tweeter.client.model.service.backgroundTask.BackgroundTaskUtils;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetFeedTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.GetStoryTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.PostStatusTask;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.PagedHandler;
import edu.byu.cs.tweeter.client.model.service.backgroundTask.handler.PostStatusHandler;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;

public class StatusService {
    public void loadMoreStories(AuthToken currUserAuthToken, User user, int pageSize, Status lastStatus, PagedObserver observer){
        GetStoryTask getStoryTask = new GetStoryTask(currUserAuthToken,
                user, pageSize, lastStatus, new PagedHandler<Status>(observer, "stories") {
        });
        BackgroundTaskUtils.runTask(getStoryTask);
    }

    public void loadMoreFeeds(AuthToken currUserAuthToken, User user, int pageSize, Status lastStatus, PagedObserver observer){
        GetFeedTask getFeedTask = new GetFeedTask(currUserAuthToken,
                user, pageSize, lastStatus, new PagedHandler<Status>(observer, "feeds") {
        });
        BackgroundTaskUtils.runTask(getFeedTask);
    }

    public void postNewPosts(AuthToken currUserAuthToken, Status newStatus, SimpleNotificationObserver observer) {
        PostStatusTask statusTask = new PostStatusTask(currUserAuthToken,
                newStatus, new PostStatusHandler(observer));
        BackgroundTaskUtils.runTask(statusTask);
    }

}
