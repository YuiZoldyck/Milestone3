package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Message;

import edu.byu.cs.tweeter.client.model.service.SimpleNotificationObserver;

public class PostStatusHandler extends BaseHandler<SimpleNotificationObserver> {

    public PostStatusHandler(SimpleNotificationObserver observer) {
        super(observer);
    }

    @Override
    protected void runSuccessAction(Message msg) {
        getObserver().actionSucceeded();
    }

    @Override
    protected void displayError(String message) {
        displayError("Failed to post status: " + message);
    }

    @Override
    protected void displayException(String ex) {
        displayException("Failed to post status because of exception: " + ex);
    }
}
