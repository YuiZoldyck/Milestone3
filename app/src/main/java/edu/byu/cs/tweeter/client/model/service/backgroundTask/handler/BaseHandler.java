package edu.byu.cs.tweeter.client.model.service.backgroundTask.handler;

import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import android.os.Handler;

import edu.byu.cs.tweeter.client.model.service.backgroundTask.BackgroundTask;

public abstract class BaseHandler<T> extends Handler {
    private final T observer;

    public T getObserver() {
        return observer;
    }
    public BaseHandler(T observer){
        super(Looper.getMainLooper());
        this.observer = observer;
    }

    @Override
    public void handleMessage(@NonNull Message msg) {
        if (msg.getData().getBoolean(BackgroundTask.SUCCESS_KEY)) {
            runSuccessAction(msg);
        } else if (msg.getData().containsKey(BackgroundTask.MESSAGE_KEY)) {
            String message = msg.getData().getString(BackgroundTask.MESSAGE_KEY);
            displayError(message);
        } else if (msg.getData().containsKey(BackgroundTask.EXCEPTION_KEY)) {
            Exception ex = (Exception) msg.getData().getSerializable(BackgroundTask.EXCEPTION_KEY);
            displayException(ex.getMessage());
        }
    }

    protected abstract void runSuccessAction(Message msg);
    protected abstract void displayError(String message);
    protected abstract void displayException(String ex);

}
