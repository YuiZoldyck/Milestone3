package edu.byu.cs.tweeter.client.model.service;

import java.util.List;

public interface PagedObserver<T> extends BaseObserver {
    void addMoreItems(List<T> items, boolean hasMorePages);
}
