package edu.byu.cs.tweeter.client.presenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.SimpleNotificationObserver;
import edu.byu.cs.tweeter.client.model.service.BoolObserver;
import edu.byu.cs.tweeter.client.model.service.CountObserver;
import edu.byu.cs.tweeter.client.model.service.IsFollowerObserver;
import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;

public class MainPresenter extends BasePresenter<MainActivityView>{
    public void updateSelectedUserFollowingAndFollowers(User user) {
        getFollowService().updateUserFollowers(Cache.getInstance().getCurrUserAuthToken(), user, new MainObserver());
        getFollowService().updateUserFollowing(Cache.getInstance().getCurrUserAuthToken(), user, new MainObserver());
    }

    public void isFollower(User user) {
        getFollowService().isFollower(Cache.getInstance().getCurrUserAuthToken(), Cache.getInstance().getCurrUser(), user, new MainObserver());
    }

    public void follow(User user) {
        getFollowService().followUser(Cache.getInstance().getCurrUserAuthToken(), user, new MainObserver());
        view.displayMessage("Adding " + user.getName() + "...");
    }

    public void unFollow(User user) {
        getFollowService().unFollowUser(Cache.getInstance().getCurrUserAuthToken(), user, new MainObserver());
        view.displayMessage("Removing " + user.getName());
    }

    public void newPost(Status newStatus) {
        getStatusService().postNewPosts(Cache.getInstance().getCurrUserAuthToken(), newStatus, new PostStatusObserver());
    }

    public String getFormattedDateTime() throws ParseException {
        SimpleDateFormat userFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat statusFormat = new SimpleDateFormat("MMM d yyyy h:mm aaa");

        return statusFormat.format(userFormat.parse(LocalDate.now().toString() + " " + LocalTime.now().toString().substring(0, 8)));
    }

    public List<String> parseURLs(String post) {
        List<String> containedUrls = new ArrayList<>();
        for (String word : post.split("\\s")) {
            if (word.startsWith("http://") || word.startsWith("https://")) {

                int index = findUrlEndIndex(word);

                word = word.substring(0, index);

                containedUrls.add(word);
            }
        }

        return containedUrls;
    }

    public List<String> parseMentions(String post) {
        List<String> containedMentions = new ArrayList<>();

        for (String word : post.split("\\s")) {
            if (word.startsWith("@")) {
                word = word.replaceAll("[^a-zA-Z0-9]", "");
                word = "@".concat(word);

                containedMentions.add(word);
            }
        }

        return containedMentions;
    }

    public int findUrlEndIndex(String word) {
        if (word.contains(".com")) {
            int index = word.indexOf(".com");
            index += 4;
            return index;
        } else if (word.contains(".org")) {
            int index = word.indexOf(".org");
            index += 4;
            return index;
        } else if (word.contains(".edu")) {
            int index = word.indexOf(".edu");
            index += 4;
            return index;
        } else if (word.contains(".net")) {
            int index = word.indexOf(".net");
            index += 4;
            return index;
        } else if (word.contains(".mil")) {
            int index = word.indexOf(".mil");
            index += 4;
            return index;
        } else {
            return word.length();
        }
    }


    public MainPresenter(MainActivityView view) {
        super(view);
    }

    public void logout() {
        view.displayMessage("Logging Out...");
        getUserService().logout(Cache.getInstance().getCurrUserAuthToken(), new LogoutObserver());
    }

    private class MainObserver implements CountObserver, BoolObserver, IsFollowerObserver {

        @Override
        public void setFollowersCount(int count) {
            view.setFollowersCount(count);
        }

        @Override
        public void setFollowingCount(int count) {
            view.setFollowingCount(count);
        }

        @Override
        public void displayError(String message) {
            view.showErrorMessage(message);
        }

        @Override
        public void displayException(Exception ex) {
            view.showErrorMessage(ex.getMessage());
        }


        @Override
        public void updateFollowButton(boolean value) {
            if(value) {
                view.setIsFollowingButton();
            } else {
                view.setIsNotFollowingButton();
            }
        }

        @Override
        public void setButton(boolean value) {
            if (value) {
                view.setIsFollowingButton();
            } else {
                view.setIsNotFollowingButton();
            }
        }
    }

    protected class PostStatusObserver implements SimpleNotificationObserver {

        @Override
        public void displayException(Exception ex) {
            view.showErrorMessage("Failed to post status: " + ex.getMessage());
        }

        @Override
        public void displayError(String message) {
            view.showErrorMessage("Failed to post status: " + message);
        }

        @Override
        public void actionSucceeded() {
            view.displayMessage("Successfully Posted!");
        }
    }
    private class LogoutObserver implements SimpleNotificationObserver {

        @Override
        public void displayException(Exception ex) {
            view.showErrorMessage("Failed to logout: " + ex.getMessage());
        }

        @Override
        public void displayError(String message) {
            view.showLogoutMessage("Failed to logout: " + message);
        }

        @Override
        public void actionSucceeded() {
            Cache.getInstance().clearCache();
            view.openMainView();
        }
    }
}
