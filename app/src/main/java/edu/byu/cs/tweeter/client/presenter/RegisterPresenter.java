package edu.byu.cs.tweeter.client.presenter;

import android.widget.ImageView;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.AuthenticateObserver;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class RegisterPresenter extends AuthenticatePresenter {


    public RegisterPresenter(AuthenticateView view) {
        super(view);
    }

    public boolean validate(String firstName, String lastName, String alias, String password, ImageView imageToUpload) {
        if (validateRegistration(firstName, lastName, alias, password, imageToUpload)) {
            view.hideErrorMessage();
            view.displayMessage("Registering...");
            return true;
        }
        return false;
    }

    public void register(String firstName, String lastName, String alias, String password, String imageToUpload) {
        var userService = getUserService();
        userService.register(firstName, lastName, alias, password, imageToUpload, this);
    }

    public boolean validateRegistration(String firstName, String lastName, String alias, String password, ImageView image) {
        if (firstName.length() == 0) {
            view.showErrorMessage("First Name cannot be empty.");
            return false;
        }
        if (lastName.length() == 0) {
            view.showErrorMessage("Last Name cannot be empty.");
            return false;
        }
        if (alias.length() == 0) {
            view.showErrorMessage("Alias cannot be empty.");
            return false;
        }
        if (alias.charAt(0) != '@') {
            view.showErrorMessage("Alias must begin with @.");
            return false;
        }
        if (alias.length() < 2) {
            view.showErrorMessage("Alias must contain 1 or more characters after the @.");
            return false;
        }
        if (password.length() == 0) {
            view.showErrorMessage("Password cannot be empty.");
            return false;
        }
        if (image.getDrawable() == null) {
            view.showErrorMessage("Profile image must be uploaded.");
            return false;
        }
        return true;
    }

}
