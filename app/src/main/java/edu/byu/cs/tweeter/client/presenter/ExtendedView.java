package edu.byu.cs.tweeter.client.presenter;

import edu.byu.cs.tweeter.model.domain.User;

public interface ExtendedView extends BaseView {
    void hideMessage();
    void hideErrorMessage();
}
