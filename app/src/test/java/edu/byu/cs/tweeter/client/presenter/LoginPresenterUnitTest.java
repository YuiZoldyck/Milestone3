package edu.byu.cs.tweeter.client.presenter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.AuthenticateObserver;
import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class LoginPresenterUnitTest {

    private static AuthenticateView mockView;
    private static UserService mockUserService;
    private static Cache mockCache;

    private static LoginPresenter loginPresenterSpy;

    @BeforeEach
    public void setup() {
        mockView = Mockito.mock(AuthenticateView.class);
        mockUserService = Mockito.mock(UserService.class);
        mockCache = Mockito.mock(Cache.class);

        loginPresenterSpy = Mockito.spy(new LoginPresenter(mockView));

        Mockito.when(loginPresenterSpy.getUserService()).thenReturn(mockUserService);

        Cache.setInstance(mockCache);
    }

    @Test
    public void testLogin_validCredentials() {
        // Arrange
        String alias = "@ValidAlias";
        String password = "ValidPassword";
        User user = new User("FirstName", "LastName", alias, "ImageURL");
        AuthToken authToken = new AuthToken("token");

        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                AuthenticateObserver observer = invocation.getArgument(2, AuthenticateObserver.class);
                observer.actionSucceeded(authToken, user);
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockUserService).login(Mockito.any(), Mockito.any(), Mockito.any());


        // Act
        loginPresenterSpy.login(alias, password);

        // Assert
        // Verify that the view methods are called as expected
        Mockito.verify(mockView).hideErrorMessage();
        Mockito.verify(mockView).displayMessage("Logging In...");

        // Verify that the cache is updated
        Mockito.verify(mockCache).setCurrUser(user);
        Mockito.verify(mockCache).setCurrUserAuthToken(authToken);

        // Verify that the view methods are called based on AuthenticateObserver callbacks
        Mockito.verify(mockView).hideErrorMessage();
        Mockito.verify(mockView).displayMessage("Hello, " + user.getName());
        Mockito.verify(mockView).openMainView(user);
    }

    @Test
    public void testLogin_loginFailed() {
        String alias = "@ValidAlias";
        String password = "ValidPassword";

        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                AuthenticateObserver observer = invocation.getArgument(2, AuthenticateObserver.class);
                observer.displayError("Login Failed");
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockUserService).login(Mockito.any(), Mockito.any(), Mockito.any());
        loginPresenterSpy.login(alias, password);
        Mockito.verify(mockView).showErrorMessage("Login Failed");
        Mockito.verify(mockCache, Mockito.times(0)).setCurrUser(Mockito.any());
        Mockito.verify(mockCache, Mockito.times(0)).setCurrUserAuthToken(Mockito.any());
    }

    @Test
    public void testLogin_loginException() {
        String alias = "@ValidAlias";
        String password = "ValidPassword";

        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                AuthenticateObserver observer = invocation.getArgument(2, AuthenticateObserver.class);
                observer.displayException(new Exception("Login Exception Thrown"));
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockUserService).login(Mockito.any(), Mockito.any(), Mockito.any());
        loginPresenterSpy.login(alias, password);
        Mockito.verify(mockView).showErrorMessage("Login Exception Thrown");
        Mockito.verify(mockCache, Mockito.times(0)).setCurrUser(Mockito.any());
        Mockito.verify(mockCache, Mockito.times(0)).setCurrUserAuthToken(Mockito.any());
    }

    @Test
    public void testLogin_invalidAlias() {
        // Arrange
        String alias = "InvalidAlias"; // Missing '@' at the beginning
        String password = "ValidPassword";

        // Act
        loginPresenterSpy.login(alias, password);

        // Assert
        // Verify that the view methods are called for the validation error
        Mockito.verify(mockView).showErrorMessage("Alias must begin with @.");

        String alias2 = ""; // Missing '@' at the beginning
        loginPresenterSpy.login(alias2, password);
        Mockito.verify(mockView).showErrorMessage("Alias must begin with @.");
    }

    @Test
    public void testLogin_invalidAliasLength() {
        // Arrange
        String alias = "@"; // Alias must contain 1 or more characters after the '@'
        String password = "ValidPassword";

        // Act
        loginPresenterSpy.login(alias, password);

        // Assert
        // Verify that the view methods are called for the validation error
        Mockito.verify(mockView).showErrorMessage("Alias must contain 1 or more characters after the @.");
    }

    @Test
    public void testLogin_emptyPassword() {
        // Arrange
        String alias = "@ValidAlias";
        String password = ""; // Empty password

        // Act
        loginPresenterSpy.login(alias, password);

        // Assert
        // Verify that the view methods are called for the validation error
        Mockito.verify(mockView).showErrorMessage("Password cannot be empty.");
    }
}
