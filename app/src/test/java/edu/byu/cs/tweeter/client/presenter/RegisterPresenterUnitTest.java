package edu.byu.cs.tweeter.client.presenter;//package edu.byu.cs.tweeter.client.presenter;
//
//import android.widget.ImageView;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//import org.mockito.invocation.InvocationOnMock;
//import org.mockito.stubbing.Answer;
//
//import edu.byu.cs.tweeter.client.cache.Cache;
//import edu.byu.cs.tweeter.client.model.service.AuthenticateObserver;
//import edu.byu.cs.tweeter.client.model.service.UserService;
//import edu.byu.cs.tweeter.model.domain.AuthToken;
//import edu.byu.cs.tweeter.model.domain.User;
//
//public class RegisterPresenterUnitTest {
//    private static ExtendedView mockView;
//    private static UserService mockUserService;
//    private static Cache mockCache;
//
//    private static RegisterPresenter registerPresenterSpy;
//
//    @BeforeEach
//    public void setup() {
//        mockView = Mockito.mock(ExtendedView.class);
//        mockUserService = Mockito.mock(UserService.class);
//        mockCache = Mockito.mock(Cache.class);
//
//        registerPresenterSpy = Mockito.spy(new RegisterPresenter(mockView));
//
//        Mockito.when(registerPresenterSpy.getUserService()).thenReturn(mockUserService);
//
//        Cache.setInstance(mockCache);
//    }
//
//
//    @Test
//    public void testRegister_loginFailed() {
//        String alias = "@ValidAlias";
//        String password = "ValidPassword";
//
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                AuthenticateObserver observer = invocation.getArgument(2, AuthenticateObserver.class);
//                observer.displayError("Login Failed");
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockUserService).login(Mockito.any(), Mockito.any(), Mockito.any());
////        registerPresenterSpy.login(alias, password);
//        Mockito.verify(mockView).showErrorMessage("Login Failed");
//        Mockito.verify(mockCache, Mockito.times(0)).setCurrUser(Mockito.any());
//        Mockito.verify(mockCache, Mockito.times(0)).setCurrUserAuthToken(Mockito.any());
//    }
//
//    @Test
//    public void testRegister_loginException() {
//        String alias = "@ValidAlias";
//        String password = "ValidPassword";
//
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                AuthenticateObserver observer = invocation.getArgument(2, AuthenticateObserver.class);
//                observer.displayException(new Exception("Login Exception Thrown"));
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockUserService).login(Mockito.any(), Mockito.any(), Mockito.any());
////        registerPresenterSpy.register(alias, password);
//        Mockito.verify(mockView).showErrorMessage("Login Exception Thrown");
//        Mockito.verify(mockCache, Mockito.times(0)).setCurrUser(Mockito.any());
//        Mockito.verify(mockCache, Mockito.times(0)).setCurrUserAuthToken(Mockito.any());
//    }
//    @Test
//    public void testRegister_successfulRegistration() {
//        // Arrange
//        String firstName = "John";
//        String lastName = "Doe";
//        String alias = "@johndoe";
//        String password = "securePassword";
//        ImageView imageToUpload = new ImageView();
//
//        // Use doAnswer to simulate a successful registration
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                AuthenticateObserver observer = invocation.getArgument(5, AuthenticateObserver.class);
//                observer.actionSucceeded(new AuthToken("token"), new User("FirstName", "LastName", alias, "ImageURL"));
//                return null;
//            }
//        };
//        Mockito.doAnswer(answer).when(mockUserService).register(firstName, lastName, alias, password, imageToUpload.toString(), registerPresenterSpy);
//
//        // Act
//        boolean result = registerPresenterSpy.validate(firstName, lastName, alias, password, null);
//        if (result) {
//            registerPresenterSpy.register(firstName, lastName, alias, password, imageToUpload.toString());
//        }
//
//        // Assert
//        // Verify that the view methods are called as expected for a successful registration
//        Mockito.verify(mockView).hideErrorMessage();
//        Mockito.verify(mockView).displayMessage("Registering...");
//        Mockito.verify(mockView).hideErrorMessage();
//        Mockito.verify(mockView).displayMessage("Hello, FirstName");
//        // You can add more verifications as needed.
//    }
//
//    @Test
//    public void testRegister_failedRegistration() {
//        // Arrange
//        String firstName = "John";
//        String lastName = "Doe";
//        String alias = "@johndoe";
//        String password = "securePassword";
//        String imageToUpload = "imageData";
//
//        // Use doAnswer to simulate a failed registration
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                AuthenticateObserver observer = invocation.getArgument(5, AuthenticateObserver.class);
//                observer.actionFailed("Registration failed");
//                return null;
//            }
//        };
//        Mockito.doAnswer(answer).when(mockUserService).register(firstName, lastName, alias, password, imageToUpload, registerPresenterSpy);
//
//        // Act
//        boolean result = registerPresenterSpy.validate(firstName, lastName, alias, password, null);
//        if (result) {
//            registerPresenterSpy.register(firstName, lastName, alias, password, imageToUpload);
//        }
//
//        // Assert
//        // Verify that the view methods are called as expected for a failed registration
//        Mockito.verify(mockView).hideErrorMessage();
//        Mockito.verify(mockView).displayMessage("Registering...");
//        Mockito.verify(mockView).showErrorMessage("Registration failed");
//        // You can add more verifications as needed.
//    }
//
//    @Test
//    public void testValidateRegistration_invalidData() {
//        // Arrange
//        String firstName = "";
//        String lastName = "";
//        String alias = "@";
//        String password = "";
//        // Set imageToUpload as null to simulate an invalid image
//
//        // Act
//        boolean result = registerPresenterSpy.validateRegistration(firstName, lastName, alias, password, null);
//
//        // Assert
//        // Verify that the view methods are called as expected for invalid registration data
//        Mockito.verify(mockView).showErrorMessage("First Name cannot be empty.");
//        Mockito.verify(mockView).showErrorMessage("Last Name cannot be empty.");
//        Mockito.verify(mockView).showErrorMessage("Alias must contain 1 or more characters after the @.");
//        Mockito.verify(mockView).showErrorMessage("Password cannot be empty.");
//        Mockito.verify(mockView).showErrorMessage("Profile image must be uploaded.");
//        // You can add more verifications as needed.
//    }
//
//    @Test
//    public void testValidateRegistration_validData() {
//        // Arrange
//        String firstName = "John";
//        String lastName = "Doe";
//        String alias = "@johndoe";
//        String password = "securePassword";
//        // Set imageToUpload as non-null to simulate a valid image
//
//        // Act
//        boolean result = registerPresenterSpy.validateRegistration(firstName, lastName, alias, password, new ImageView(Mockito.any()));
//
//        // Assert
//        // Verify that the view methods are called as expected for valid registration data
//        Mockito.verify(mockView).hideErrorMessage();
//        // You can add more verifications as needed.
//    }
//}
