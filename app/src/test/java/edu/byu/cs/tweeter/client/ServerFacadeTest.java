package edu.byu.cs.tweeter.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import edu.byu.cs.tweeter.client.model.net.ServerFacade;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;
import edu.byu.cs.tweeter.model.net.TweeterRemoteException;
import edu.byu.cs.tweeter.model.net.request.CountRequest;
import edu.byu.cs.tweeter.model.net.request.PagedUserRequest;
import edu.byu.cs.tweeter.model.net.request.RegisterRequest;
import edu.byu.cs.tweeter.model.net.response.AuthenticateResponse;
import edu.byu.cs.tweeter.model.net.response.CountResponse;
import edu.byu.cs.tweeter.model.net.response.PagedUserResponse;
import edu.byu.cs.tweeter.util.FakeData;

public class ServerFacadeTest {
    private ServerFacade serverFacade = new ServerFacade();

    @Test
    public void register_success() throws IOException, TweeterRemoteException {
        RegisterRequest request = new RegisterRequest(
                "Allen", "Anderson", "@allen", "asdfdsa", "https://faculty.cs.byu.edu/~jwilkerson/cs340/tweeter/images/donald_duck.png");
        AuthenticateResponse response = serverFacade.register(request, "/register");

        FakeData fakeData = FakeData.getInstance();
        Assertions.assertEquals(fakeData.getFirstUser(), response.getUser());
    }

    @Test
    public void count_success() throws IOException, TweeterRemoteException {
        User testUser = new User("Allen", "Anderson", "@allen", "https://faculty.cs.byu.edu/~jwilkerson/cs340/tweeter/images/donald_duck.png");
        AuthToken authToken = new AuthToken("asdfdsadsfdsa");
        CountRequest request = new CountRequest(authToken, testUser);
        CountResponse response = serverFacade.getCount(request, "/getfollowercount");

        FakeData fakeData = FakeData.getInstance();
        Assertions.assertEquals(21, response.getCount());
    }

    @Test
    public void getFollowers_success() throws IOException, TweeterRemoteException {
        AuthToken authToken = new AuthToken("asdfdsadsfdsa");
        PagedUserRequest request = new PagedUserRequest(authToken, "@allen", 3, null);
        PagedUserResponse response = serverFacade.getFollowees(request, "/getfollower");

        FakeData fakeData = FakeData.getInstance();
        Assertions.assertEquals(fakeData.getPageOfUsers(null, 3, null).getFirst(), response.getItems());

    }
}
