package edu.byu.cs.tweeter.client.presenter;

import static org.mockito.ArgumentMatchers.eq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.PagedObserver;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class FollowingPresenterUnitTest {

    private static PagedView<User> mockView;
    private static FollowService mockFollowService;
    private static FollowingPresenter followingPresenterSpy;

    private static Cache mockCache;

    @BeforeEach
    public void setUp() {
        mockView = Mockito.mock(PagedView.class);
        mockFollowService = Mockito.mock(FollowService.class);
        mockCache = Mockito.mock(Cache.class);

        followingPresenterSpy = Mockito.spy(new FollowingPresenter(mockView));

        Mockito.when(followingPresenterSpy.getFollowService()).thenReturn(mockFollowService);
        Cache.setInstance(mockCache);
    }

    @Test
    public void testLoadMoreItems_Success() {
        AuthToken authToken = new AuthToken("exampleAuthToken");
        User user = new User("userAlias", "FirstName", "LastName", "ImageURL");

        List<User> users = new ArrayList<>();
        users.add(new User("user1", "John", "Doe", "image1"));
        users.add(new User("user2", "Jane", "Smith", "image2"));

        Answer<Void> answer = new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                PagedObserver observer = invocation.getArgument(5, PagedObserver.class);
                observer.addMoreItems(users, true);
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockFollowService).loadMoreItemsFollowing(Mockito.any(), Mockito.any(), Mockito.anyInt(), Mockito.any(), Mockito.any());

        followingPresenterSpy.loadMoreItems(user);

        Mockito.verify(mockView).setLoadingFooter(false);
        Mockito.verify(mockView).addMoreItems(users);
    }

//    @Test
//    public void testLoadMoreItems_Failure() {
//        AuthToken authToken = new AuthToken("exampleAuthToken");
//        User user = new User("userAlias", "FirstName", "LastName", "ImageURL");
//
//        when(mockUserService.loadMoreItemsFollowing(eq(authToken), eq(user), eq(FollowingPresenter.getPageSize()), any(User.class), any(ServiceObserver.class)))
//                .thenAnswer(new Answer<Void>() {
//                    @Override
//                    public Void answer(InvocationOnMock invocation) throws Throwable {
//                        ServiceObserver observer = invocation.getArgument(4, ServiceObserver.class);
//                        observer.displayError("Failed to load more items.");
//                        return null;
//                    }
//                });
//
//        followingPresenter.loadMoreItems(user);
//
//        verify(mockView).setLoadingFooter(false);
//        verify(mockView).showErrorMessage("Failed to load more items.");
//    }



}
