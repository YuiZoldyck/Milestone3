package edu.byu.cs.tweeter.client.presenter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

import edu.byu.cs.tweeter.client.cache.Cache;
import edu.byu.cs.tweeter.client.model.service.FollowService;
import edu.byu.cs.tweeter.client.model.service.SimpleNotificationObserver;
import edu.byu.cs.tweeter.client.model.service.StatusService;
import edu.byu.cs.tweeter.client.model.service.UserService;
import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;

public class MainPresenterUnitTest {

    private static MainActivityView mockView;
    private static UserService mockUserService;
    private static FollowService mockFollowService;
    private static StatusService mockStatusService;
    private static Cache mockCache;

    private static MainPresenter mainPresenterSpy;

    User mockUser = new User("FirstName", "LastName", "url");
    Status status = new Status("Test", mockUser, (long) 1212341345, new ArrayList<String>(), new ArrayList<String>());

    @BeforeEach
    public void setup() {
        // Create mocks
        mockView = Mockito.mock(MainActivityView.class);
        mockUserService = Mockito.mock(UserService.class);
        mockFollowService = Mockito.mock(FollowService.class);
        mockStatusService = Mockito.mock(StatusService.class);
        mockCache = Mockito.mock(Cache.class);

        mainPresenterSpy = Mockito.spy(new MainPresenter(mockView));
        // no type checking, works for void
//        Mockito.doReturn(mockUserService).when(mainPresenterSpy).getUserService();
        // better overall
        Mockito.when(mainPresenterSpy.getUserService()).thenReturn(mockUserService);
        Mockito.when(mainPresenterSpy.getFollowService()).thenReturn(mockFollowService);
        Mockito.when(mainPresenterSpy.getStatusService()).thenReturn(mockStatusService);

        Cache.setInstance(mockCache);
    }

    @Test
    public void testPostStatus_newPostSucceeded() {
        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Assertions.assertEquals(invocation.getArgument(1).getClass(), Status.class);

                // Check if the Status object being posted is the same as the one you created
                Status testStatus = invocation.getArgument(1, Status.class);
                Assertions.assertEquals(status, testStatus);

                Assertions.assertEquals(invocation.getArgument(2).getClass(), MainPresenter.PostStatusObserver.class);
                SimpleNotificationObserver observer = invocation.getArgument(2, SimpleNotificationObserver.class);
                observer.actionSucceeded();
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockStatusService).postNewPosts(Mockito.any(), Mockito.any(), Mockito.any());
        mainPresenterSpy.newPost(status);

        Mockito.verify(mockStatusService).postNewPosts(Mockito.any(), Mockito.eq(status), Mockito.any());
        Mockito.verify(mockView).displayMessage("Successfully Posted!");
    }

    @Test
    public void testPostStatus_newPostError() {
        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Assertions.assertEquals(invocation.getArgument(2).getClass(), MainPresenter.PostStatusObserver.class);
                SimpleNotificationObserver observer = invocation.getArgument(2, SimpleNotificationObserver.class);
                observer.displayError("Status Post Failed");
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockStatusService).postNewPosts(Mockito.any(), Mockito.any(), Mockito.any());
        mainPresenterSpy.newPost(status);

        Mockito.verify(mockStatusService).postNewPosts(Mockito.any(), Mockito.eq(status), Mockito.any());
        Mockito.verify(mockView).showErrorMessage("Failed to post status: Status Post Failed");
    }

    @Test
    public void testPostStatus_newPostException() {
        Answer<Void> answer = new Answer<>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Assertions.assertEquals(invocation.getArgument(2).getClass(), MainPresenter.PostStatusObserver.class);
                SimpleNotificationObserver observer = invocation.getArgument(2, SimpleNotificationObserver.class);
                observer.displayException(new Exception("Status Post Exception Thrown"));
                return null;
            }
        };

        Mockito.doAnswer(answer).when(mockStatusService).postNewPosts(Mockito.any(), Mockito.any(), Mockito.any());
        mainPresenterSpy.newPost(status);

        Mockito.verify(mockStatusService).postNewPosts(Mockito.any(), Mockito.any(), Mockito.any());
        Mockito.verify(mockView).showErrorMessage("Failed to post status: Status Post Exception Thrown");
    }

//    @Test
//    public void testLogout_logoutSucceeded() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                LogoutObserver observer = invocation.getArgument(1, LogoutObserver.class);
//                observer.logoutSucceeded();
//                return null;
//            }
//        };
//
//
//        Mockito.doAnswer(answer).when(mockUserService).logout(Mockito.any(), Mockito.any());
//        mainPresenterSpy.logout();
//        Mockito.verify(mockView).displayMessage("Logging Out...");
//        Mockito.verify(mockUserService).logout(Mockito.any(), Mockito.any()); // Verify that logout was called
//        Mockito.verify(mockView).hideLogoutMessage();
//        Mockito.verify(mockCache).clearCache();
//        Mockito.verify(mockView).openMainView();
//    }
//
//    // service -> task  -> presenter -> view
//
//    @Test
//    public void testLogout_failed() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                LogoutObserver observer = invocation.getArgument(1, LogoutObserver.class);
//                observer.showLogoutMessage("this error");
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockUserService).logout(Mockito.any(), Mockito.any());
//        mainPresenterSpy.logout();
//        Mockito.verify(mockView).displayMessage("Logging Out...");
//        Mockito.verify(mockUserService).logout(Mockito.any(), Mockito.any()); // Verify that logout was called
//        Mockito.verify(mockCache, Mockito.times(0)).clearCache();
//        Mockito.verify(mockView).showLogoutMessage("Failed to logout: this error");
//    }
//
//    @Test
//    public void testLogout_exception() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                LogoutObserver observer = invocation.getArgument(1, LogoutObserver.class);
//                observer.displayException(new Exception("Exception Thrown"));
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockUserService).logout(Mockito.any(), Mockito.any());
//        mainPresenterSpy.logout();
//        Mockito.verify(mockView).displayMessage("Logging Out...");
//        Mockito.verify(mockUserService).logout(Mockito.any(), Mockito.any()); // Verify that logout was called
//        Mockito.verify(mockCache, Mockito.times(0)).clearCache();
//        Mockito.verify(mockView).showErrorMessage("Exception Thrown");
//
//    }
//
//    @Test
//    public void testFollow_followSucceeded() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.updateFollowButton(true);
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User userToFollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.follow(userToFollow);
//
//        Mockito.verify(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView).setIsFollowingButton();
//        Mockito.verify(mockView).displayMessage("Adding " + userToFollow.getName() + "...");
//
//        // You can add more verifications as needed, like verifying UI updates.
//    }
//
//    @Test
//    public void testFollow_followFailed() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.displayError("Failed to follow");
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User userToFollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.follow(userToFollow);
//
//        Mockito.verify(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView, Mockito.times(0)).setIsFollowingButton();
//        Mockito.verify(mockView).showErrorMessage("Failed to follow");
//
//    }
//
//    @Test
//    public void testFollow_followException() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.displayException(new Exception("Exception Thrown"));
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User userToFollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.follow(userToFollow);
//
//        Mockito.verify(mockFollowService).followUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView).showErrorMessage("Exception Thrown");
//    }
//
//    @Test
//    public void testUnfollow_unfollowSucceeded() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.updateFollowButton(false);
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User usertoUnfollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.unFollow(usertoUnfollow);
//
//        Mockito.verify(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView).setIsNotFollowingButton();
//        Mockito.verify(mockView).displayMessage("Removing " + usertoUnfollow.getName());
//    }
//
//    @Test
//    public void testUnfollow_unfollowFailed() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.displayError("Unfollow Failed");
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User usertoUnfollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.unFollow(usertoUnfollow);
//
//        Mockito.verify(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView, Mockito.times(0)).setIsNotFollowingButton();
//        Mockito.verify(mockView).showErrorMessage("Unfollow Failed");
//    }
//
//    @Test
//    public void testUnfollow_unfollowException() {
//        Answer<Void> answer = new Answer<>() {
//            @Override
//            public Void answer(InvocationOnMock invocation) throws Throwable {
//                BoolObserver observer = invocation.getArgument(2, BoolObserver.class);
//                observer.displayException(new Exception("Unfollow Exception Thrown"));
//                return null;
//            }
//        };
//
//        Mockito.doAnswer(answer).when(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//
//        User usertoUnfollow = new User("UsernameToFollow", "FirstName", "LastName", "ImageURL");
//
//        mainPresenterSpy.unFollow(usertoUnfollow);
//
//        Mockito.verify(mockFollowService).unFollowUser(Mockito.any(), Mockito.any(), Mockito.any());
//        Mockito.verify(mockView, Mockito.times(0)).setIsNotFollowingButton();
//        Mockito.verify(mockView).showErrorMessage("Unfollow Exception Thrown");
//    }

}
