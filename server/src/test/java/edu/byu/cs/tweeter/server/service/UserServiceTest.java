package edu.byu.cs.tweeter.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.byu.cs.tweeter.model.net.request.RegisterRequest;
import edu.byu.cs.tweeter.model.net.response.AuthenticateResponse;


public class UserServiceTest {

    private UserService userService;

    @BeforeEach
    public void setup() {
        userService = new UserService();
    }

    @Test
    public void testRegister_validRequest_correctResponse() {
        RegisterRequest registerRequest = new RegisterRequest("username", "password", "John", "Doe", "imageURL");
        AuthenticateResponse response = userService.register(registerRequest);

        // Check that the response is not null
        Assertions.assertNotNull(response);

        // Check that the user in the response is not null
        Assertions.assertNotNull(response.getUser());

        // Check that the authToken in the response is not null
        Assertions.assertNotNull(response.getAuthToken());
    }
}

