package edu.byu.cs.tweeter.server.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;
import edu.byu.cs.tweeter.model.net.request.CountRequest;
import edu.byu.cs.tweeter.model.net.request.FollowRequest;
import edu.byu.cs.tweeter.model.net.request.IsFollowerRequest;
import edu.byu.cs.tweeter.model.net.request.PagedUserRequest;
import edu.byu.cs.tweeter.model.net.request.UnfollowRequest;
import edu.byu.cs.tweeter.model.net.response.CountResponse;
import edu.byu.cs.tweeter.model.net.response.FollowResponse;
import edu.byu.cs.tweeter.model.net.response.IsFollowerResponse;
import edu.byu.cs.tweeter.model.net.response.PagedUserResponse;
import edu.byu.cs.tweeter.model.net.response.UnfollowResponse;
import edu.byu.cs.tweeter.server.dao.FollowDAO;
import edu.byu.cs.tweeter.util.Pair;

public class FollowServiceTest {

    private PagedUserRequest request;
    private PagedUserResponse expectedResponse;
    private FollowDAO mockFollowDAO;
    private FollowService followServiceSpy;
    private CountRequest countRequest;
    private FollowRequest followRequest;
    private UnfollowRequest unfollowRequest;
    private IsFollowerRequest isFollowerRequest;

    @BeforeEach
    public void setup() {
        AuthToken authToken = new AuthToken();

        User currentUser = new User("FirstName", "LastName", null);

        User resultUser1 = new User("FirstName1", "LastName1",
                "https://faculty.cs.byu.edu/~jwilkerson/cs340/tweeter/images/donald_duck.png");
        User resultUser2 = new User("FirstName2", "LastName2",
                "https://faculty.cs.byu.edu/~jwilkerson/cs340/tweeter/images/daisy_duck.png");
        User resultUser3 = new User("FirstName3", "LastName3",
                "https://faculty.cs.byu.edu/~jwilkerson/cs340/tweeter/images/daisy_duck.png");

        // Setup a request object to use in the tests
        request = new PagedUserRequest(authToken, currentUser.getAlias(), 3, null);

        // Setup a mock FollowDAO that will return known responses
        expectedResponse = new PagedUserResponse(false, Arrays.asList(resultUser1, resultUser2, resultUser3));
        mockFollowDAO = Mockito.mock(FollowDAO.class);
        Mockito.when(mockFollowDAO.getFollowees(request.getUser1(), request.getLimit(), request.getUser2()))
                .thenReturn(new Pair<>(expectedResponse.getItems(), expectedResponse.getHasMorePages()));

        countRequest = new CountRequest(authToken, currentUser);
        followRequest = new FollowRequest(authToken, "followerUser");
        unfollowRequest = new UnfollowRequest(authToken, "followerUser");
        isFollowerRequest = new IsFollowerRequest(authToken, "followerUser", "followeeUser");

        followServiceSpy = Mockito.spy(FollowService.class);
        Mockito.when(followServiceSpy.getFollowingDAO()).thenReturn(mockFollowDAO);
    }

    /**
     * Verify that the {@link FollowService#getFollowees(PagedUserRequest)}
     * method returns the same result as the {@link FollowDAO} class.
     */
    @Test
    public void testGetFollowees_validRequest_correctResponse() {
        PagedUserResponse response = followServiceSpy.getFollowees(request);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetFollowers_validRequest_correctResponse() {
        // Use the same setup as getFollowees, just changing the method and request
        PagedUserResponse response = followServiceSpy.getFollowers(request);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetFollowingCount_validRequest_correctResponse() {

        // Setup a mock FollowDAO that will return a known count
        int expectedCount = 5;
        Mockito.when(mockFollowDAO.getFolloweeCount(countRequest.getUser()))
                .thenReturn(expectedCount);

        // Call the method and check the response
        CountResponse response = followServiceSpy.getFollowingCount(countRequest);
        Assertions.assertEquals(expectedCount, response.getCount());
    }

    @Test
    public void testGetFollowerCount_validRequest_correctResponse() {

        // Setup a mock FollowDAO that will return a known count
        int expectedCount = 8;
        Mockito.when(mockFollowDAO.getFolloweeCount(countRequest.getUser()))
                .thenReturn(expectedCount);

        // Call the method and check the response
        CountResponse response = followServiceSpy.getFollowerCount(countRequest);
        Assertions.assertEquals(expectedCount, response.getCount());
    }
}
