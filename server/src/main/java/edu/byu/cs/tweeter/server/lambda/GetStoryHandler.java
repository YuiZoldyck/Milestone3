package edu.byu.cs.tweeter.server.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import edu.byu.cs.tweeter.model.net.request.PagedStatusRequest;
import edu.byu.cs.tweeter.model.net.response.PagedStatusResponse;
import edu.byu.cs.tweeter.server.service.StatusService;

public class GetStoryHandler implements RequestHandler<PagedStatusRequest, PagedStatusResponse> {
    @Override
    public PagedStatusResponse handleRequest(PagedStatusRequest storyRequest, Context context) {
        StatusService statusService = new StatusService();
        return statusService.getStories(storyRequest);
    }
}
