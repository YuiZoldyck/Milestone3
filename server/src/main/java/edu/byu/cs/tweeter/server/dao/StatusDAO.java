package edu.byu.cs.tweeter.server.dao;

import java.util.ArrayList;
import java.util.List;

import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.domain.User;
import edu.byu.cs.tweeter.util.FakeData;
import edu.byu.cs.tweeter.util.Pair;

public class StatusDAO {

    public Pair<List<Status>, Boolean> getStatuses(String targetUser, int limit, Status lastStatus) {
        // TODO: Generates dummy data. Replace with a real implementation.
        assert limit > 0;
        assert targetUser != null;

        List<Status> allStatuses = getDummyStatuses();
        List<Status> responseStatuses = new ArrayList<>(limit);

        boolean hasMorePages = false;

        if(limit > 0) {
            if (allStatuses != null) {
                int statusesIndex = getStatusesStartingIndex(lastStatus, allStatuses);
                System.out.println(allStatuses);

                for(int limitCounter = 0; statusesIndex < allStatuses.size() && limitCounter < limit; statusesIndex++, limitCounter++) {
                    responseStatuses.add(allStatuses.get(statusesIndex));
                }

                hasMorePages = statusesIndex < allStatuses.size();
            }
        }
//        System.out.println("Current responsese " + allStatuses);

        return new Pair<>(responseStatuses, hasMorePages);
    }

    private int getStatusesStartingIndex(Status lastStatus, List<Status> allStatuses) {

        int statusesIndex = 0;

        System.out.println("all status" + allStatuses);
        if(lastStatus != null) {
            System.out.println("Last Status" +lastStatus);
            // This is a paged request for something after the first page. Find the first item
            // we should return
            for (int i = 0; i < allStatuses.size(); i++) {
                if(lastStatus.equals(allStatuses.get(i))) {
                    System.out.println("HEREREERER" + lastStatus);
                    System.out.println("stuff" + allStatuses.get(i).getPost());
                    // We found the index of the last item returned last time. Increment to get
                    // to the first one we should return
                    statusesIndex = i + 1;
                    break;
                }
            }
        }
        System.out.println("Status Index: " + statusesIndex);

        return statusesIndex;
    }

    /**
     * Returns the list of dummy followee data. This is written as a separate method to allow
     * mocking of the followees.
     *
     * @return the followees.
     */
    List<Status> getDummyStatuses() {
        return getFakeData().getFakeStatuses();
    }

    /**
     * Returns the {@link FakeData} object used to generate dummy followees.
     * This is written as a separate method to allow mocking of the {@link FakeData}.
     *
     * @return a {@link FakeData} instance.
     */
    FakeData getFakeData() {
        return FakeData.getInstance();
    }

}
