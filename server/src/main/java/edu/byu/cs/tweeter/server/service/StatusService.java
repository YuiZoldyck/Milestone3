package edu.byu.cs.tweeter.server.service;

import java.util.List;

import edu.byu.cs.tweeter.model.domain.Status;
import edu.byu.cs.tweeter.model.net.request.PagedStatusRequest;
import edu.byu.cs.tweeter.model.net.request.PostStatusRequest;
import edu.byu.cs.tweeter.model.net.response.PagedStatusResponse;
import edu.byu.cs.tweeter.model.net.response.PostStatusResponse;
import edu.byu.cs.tweeter.server.dao.StatusDAO;
import edu.byu.cs.tweeter.util.Pair;

public class StatusService {
    public PagedStatusResponse getStories(PagedStatusRequest request) {
        if(request.getTargetUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        } else if(request.getLimit() <= 0) {
            throw new RuntimeException("[Bad Request] Request needs to have a positive limit");
        }

        Pair<List<Status>, Boolean> pair = getStatusDAO().getStatuses(request.getTargetUser(), request.getLimit(), request.getLastStatus());
        return new PagedStatusResponse(pair.getSecond(), pair.getFirst());
    }

    public PagedStatusResponse getFeed(PagedStatusRequest request) {
        if(request.getTargetUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        } else if(request.getLimit() <= 0) {
            throw new RuntimeException("[Bad Request] Request needs to have a positive limit");
        }

        Pair<List<Status>, Boolean> pair = getStatusDAO().getStatuses(request.getTargetUser(), request.getLimit(), request.getLastStatus());
//        System.out.println(pair.getFirst());
        return new PagedStatusResponse(pair.getSecond(), pair.getFirst());
    }

    public PostStatusResponse postStatus(PostStatusRequest request) {
        if(request.getPost() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a post");
        }
        return new PostStatusResponse();
    }

    StatusDAO getStatusDAO() {return new StatusDAO();}
}
