package edu.byu.cs.tweeter.server.service;

import java.util.List;

import edu.byu.cs.tweeter.model.domain.User;
import edu.byu.cs.tweeter.model.net.request.CountRequest;
import edu.byu.cs.tweeter.model.net.request.FollowRequest;
import edu.byu.cs.tweeter.model.net.request.IsFollowerRequest;
import edu.byu.cs.tweeter.model.net.request.PagedUserRequest;
import edu.byu.cs.tweeter.model.net.request.UnfollowRequest;
import edu.byu.cs.tweeter.model.net.response.CountResponse;
import edu.byu.cs.tweeter.model.net.response.FollowResponse;
import edu.byu.cs.tweeter.model.net.response.IsFollowerResponse;
import edu.byu.cs.tweeter.model.net.response.PagedUserResponse;
import edu.byu.cs.tweeter.model.net.response.UnfollowResponse;
import edu.byu.cs.tweeter.server.dao.FollowDAO;
import edu.byu.cs.tweeter.util.Pair;

/**
 * Contains the business logic for getting the users a user is following.
 */
public class FollowService {

    /**
     * Returns the users that the user specified in the request is following. Uses information in
     * the request object to limit the number of followees returned and to return the next set of
     * followees after any that were returned in a previous request. Uses the {@link FollowDAO} to
     * get the followees.
     *
     * @param request contains the data required to fulfill the request.
     * @return the followees.
     */
    public PagedUserResponse getFollowees(PagedUserRequest request) {
        if(request.getUser1() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a follower alias");
        } else if(request.getLimit() <= 0) {
            throw new RuntimeException("[Bad Request] Request needs to have a positive limit");
        }

        Pair<List<User>, Boolean> pair = getFollowingDAO().getFollowees(request.getUser1(), request.getLimit(), request.getUser2());
        return new PagedUserResponse(pair.getSecond(), pair.getFirst());
    }

    public PagedUserResponse getFollowers(PagedUserRequest request) {
        if(request.getUser1() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a followee alias");
        } else if(request.getLimit() <= 0) {
            throw new RuntimeException("[Bad Request] Request needs to have a positive limit");
        }

        Pair<List<User>, Boolean> pair = getFollowingDAO().getFollowees(request.getUser1(), request.getLimit(), request.getUser2());
        return new PagedUserResponse(pair.getSecond(), pair.getFirst());
    }

    public CountResponse getFollowingCount(CountRequest request) {
        if(request.getUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        }

        int count = getFollowingDAO().getFolloweeCount(request.getUser());
        return new CountResponse(count);
    }

    public CountResponse getFollowerCount(CountRequest request) {
        if(request.getUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        }

        int count = getFollowingDAO().getFolloweeCount(request.getUser());
        return new CountResponse(count);
    }

    public FollowResponse follow(FollowRequest request) {
        if(request.getUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        }

        return new FollowResponse();
    }

    public UnfollowResponse unfollow(UnfollowRequest request) {
        if(request.getUser() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a user");
        }
        return new UnfollowResponse();
    }

    public IsFollowerResponse isFollower(IsFollowerRequest request) {
        if(request.getFollowerAlias() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a valid follower alias");
        } else if(request.getFolloweeAlias() == null) {
            throw new RuntimeException("[Bad Request] Request needs to have a valid followee alias");
        }

        boolean isFollower = getFollowingDAO().isFollower(request.getFolloweeAlias());
        return new IsFollowerResponse(isFollower);
    }
    /**
     * Returns an instance of {@link FollowDAO}. Allows mocking of the FollowDAO class
     * for testing purposes. All usages of FollowDAO should get their FollowDAO
     * instance from this method to allow for mocking of the instance.
     *
     * @return the instance.
     */
    FollowDAO getFollowingDAO() {
        return new FollowDAO();
    }
}
