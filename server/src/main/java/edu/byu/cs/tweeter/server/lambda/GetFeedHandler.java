package edu.byu.cs.tweeter.server.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import edu.byu.cs.tweeter.model.net.request.PagedStatusRequest;
import edu.byu.cs.tweeter.model.net.response.PagedStatusResponse;
import edu.byu.cs.tweeter.server.service.StatusService;

public class GetFeedHandler implements RequestHandler<PagedStatusRequest, PagedStatusResponse> {
    @Override
    public PagedStatusResponse handleRequest(PagedStatusRequest feedRequest, Context context) {
        StatusService statusService = new StatusService();
        System.out.println(feedRequest.getLastStatus());
        return statusService.getFeed(feedRequest);
    }
}
