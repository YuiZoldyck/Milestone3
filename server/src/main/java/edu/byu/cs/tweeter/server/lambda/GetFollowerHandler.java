package edu.byu.cs.tweeter.server.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import edu.byu.cs.tweeter.model.net.request.PagedUserRequest;
import edu.byu.cs.tweeter.model.net.response.PagedUserResponse;
import edu.byu.cs.tweeter.server.service.FollowService;

public class GetFollowerHandler implements RequestHandler <PagedUserRequest, PagedUserResponse> {


    @Override
    public PagedUserResponse handleRequest(PagedUserRequest followerRequest, Context context) {
        FollowService followService = new FollowService();
        return followService.getFollowers(followerRequest);
    }
}
