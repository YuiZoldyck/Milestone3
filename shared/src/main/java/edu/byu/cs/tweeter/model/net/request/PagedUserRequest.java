package edu.byu.cs.tweeter.model.net.request;

import edu.byu.cs.tweeter.model.domain.AuthToken;
import edu.byu.cs.tweeter.model.domain.User;

public class PagedUserRequest {
    private AuthToken authToken;
    private String user1;
    private int limit;
    private User user2;

    private PagedUserRequest() {}

    public PagedUserRequest(AuthToken authToken, String user1, int limit, User user2) {
        this.authToken = authToken;
        this.user1 = user1;
        this.limit = limit;
        this.user2 = user2;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }

    public String getUser1() {
        return user1;
    }

    public void setUser1(String user) {
        this.user1 = user;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }
}
