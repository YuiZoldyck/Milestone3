package edu.byu.cs.tweeter.model.net.response;

import java.util.List;
import java.util.Objects;

import edu.byu.cs.tweeter.model.domain.User;

public class PagedUserResponse extends PagedResponse<User>{

    public PagedUserResponse(String message) {super(false, message,  false, null);}
    public PagedUserResponse(boolean hasMorePages, List<User> users) {
        super(true, hasMorePages, users);
    }
}
