package edu.byu.cs.tweeter.model.net.response;

import java.util.List;
import java.util.Objects;

import edu.byu.cs.tweeter.model.domain.Status;

public class PagedStatusResponse extends PagedResponse<Status> {
    public PagedStatusResponse(String message) {super(false, message, false, null);}
    public PagedStatusResponse(boolean hasMorePages, List<Status> statuses) {
        super(true, hasMorePages, statuses);
    }
}
